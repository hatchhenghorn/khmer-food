// Intro Loader
function Loader(){
    document.querySelector('.intro-container').classList.add('fade-out');
}

function fadeOut(){
    setInterval(Loader, 3000);
}

window.onload = fadeOut;

// Navigation
let menuBtn = document.querySelector('#menu-bar');
let navBar = document.querySelector('.navbar');

menuBtn.onclick = () =>{
    menuBtn.classList.toggle('active');
    navBar.classList.toggle('active');
}

// DarkMode
let themeBtn = document.querySelector('#theme-btn');

themeBtn.onclick = () =>{
    themeBtn.classList.toggle('fa-sun');

    if(themeBtn.classList.contains('fa-sun')){
        document.body.classList.add('active');
    }else{
        document.body.classList.remove('active');
    }

}

// Color Switch
let colorsPalette = document.querySelector('.colors-palette');

document.querySelector('#color-btn').onclick = () =>{
    colorsPalette.classList.toggle('active');
}

document.querySelectorAll('.colors-palette .color').forEach(btn =>{
    btn.onclick = () =>{
        let color = btn.style.background;
        document.querySelector(':root').style.setProperty('--main-color', color);
    }
});


// login & Signup Form
let LoginForm = document.querySelector('.main-container'),
background = document.querySelector('.background');

document.querySelector('#login-btn').onclick = () =>{
    LoginForm.classList.add('active');
    background.classList.add('active');
}

document.querySelector('.close').onclick = () =>{
    LoginForm.classList.remove('active');
    background.classList.remove('active');
}

// Added Cart
let addCart = document.querySelector('.cart-container');
let background1 = document.querySelector('.background1');

document.querySelector('#shop-cart').onclick = () =>{
    addCart.classList.add('active');
}

document.querySelector('.closeCart').onclick = () =>{
    addCart.classList.remove('active');
}

document.querySelector('#cancel').onclick = () =>{
    addCart.classList.remove('active');
}

// Payment
let payment = document.querySelector('.payment-container');

document.querySelector('#payMoney').onclick = () =>{
    payment.classList.add('active');
}

document.querySelector('.closePayment').onclick = () =>{
    payment.classList.remove('active');
}

// Windows Onscroll

// scroll spy

let section = document.querySelectorAll('section');
let navlinks = document.querySelectorAll('.header-2 .navbar a');

window.onscroll = () =>{

    colorsPalette.classList.remove('active');
    menuBtn.classList.remove('active');
    navBar.classList.remove('active');

    if(window.scrollY > 80){
        document.querySelector('.header-2').classList.add('active');
        if(window.scrollY > 500){
            document.querySelector('#scroll-top').classList.add('active');
        }else{
            document.querySelector('#scroll-top').classList.remove('active');
        }
    }else{
        document.querySelector('.header-2').classList.remove('active');
    }

    section.forEach(sec =>{
        let top = window.scrollY;
        let offset = sec.offsetTop - 200;
        let height = sec.offsetHeight;
        let id = sec.getAttribute('id');

        if(top >= offset && top < offset + height){
            navlinks.forEach(link =>{
                link.classList.remove('active');
                document.querySelector('.header-2 .navbar a[href*='+id+']').classList.add('active');
            })
        }
    });
    
}

// Like
let Like = document.querySelector('.like-content');

document.querySelector('#heart-btn').onclick = () =>{
    Like.classList.add('active');
}

document.querySelector('.closeLike').onclick = () =>{
    Like.classList.remove('active');
}

document.querySelector('#cancelLike').onclick = () =>{
    Like.classList.remove('active');
}

// Views Details
let viewsDetail = document.querySelector('.views-detail');
let viewsDetail1 = document.querySelector('.views-detail1');
let viewsDetail2 = document.querySelector('.views-detail2');
let viewsDetail3 = document.querySelector('.views-detail3');
let viewsDetail4 = document.querySelector('.views-detail4');
let viewsDetail5 = document.querySelector('.views-detail5');

document.querySelector('#viewD1').onclick = () =>{
    viewsDetail.classList.add('active');
}

document.querySelector('#viewD2').onclick = () =>{
    viewsDetail1.classList.add('active');
}

document.querySelector('#viewD3').onclick = () =>{
    viewsDetail2.classList.add('active');
}

document.querySelector('#viewD4').onclick = () =>{
    viewsDetail3.classList.add('active');
}

document.querySelector('#viewD5').onclick = () =>{
    viewsDetail4.classList.add('active');
}

document.querySelector('#viewD6').onclick = () =>{
    viewsDetail5.classList.add('active');
}

// Close Views

document.querySelector('.closeDetail').onclick = () =>{
    viewsDetail.classList.remove('active');
}

document.querySelector('.closeDetail1').onclick = () =>{
    viewsDetail1.classList.remove('active');
}

document.querySelector('.closeDetail2').onclick = () =>{
    viewsDetail2.classList.remove('active');
}

document.querySelector('.closeDetail3').onclick = () =>{
    viewsDetail3.classList.remove('active');
}

document.querySelector('.closeDetail4').onclick = () =>{
    viewsDetail4.classList.remove('active');
}

document.querySelector('.closeDetail5').onclick = () =>{
    viewsDetail5.classList.remove('active');
}

// Home Slider
let slides = document.querySelectorAll('.slide-container');
let index = 0;

// Nextfuntion
function next(){
    slides[index].classList.remove('active');
    index = (index + 1) % slides.length;
    slides[index].classList.add('active'); 
}

// Previous
function prev(){
    slides[index].classList.remove('active');
    index = (index - 1 + slides.length) % slides.length;
    slides[index].classList.add('active');
}

// Auto Play
setInterval (next, 7000);

// Menu
let list = document.querySelectorAll('.list');
let itemBox = document.querySelectorAll('.item-box');

for(let i = 0; i<list.length; i++){
    list[i].addEventListener('click', function(){
        for(let j = 0; j<list.length; j++){
            list[j].classList.remove('active');
        }
        this.classList.add('active');

        let dataFilter = this.getAttribute('data-filter');

        for(let k = 0; k<itemBox.length; k++){
            itemBox[k].classList.remove('active');
            itemBox[k].classList.add('hide');

            if(itemBox[k].getAttribute('data-item') == dataFilter ||
            dataFilter == "all"){
                itemBox[k].classList.remove('hide');
                itemBox[k].classList.add('active');
            }
        }

    })
}



